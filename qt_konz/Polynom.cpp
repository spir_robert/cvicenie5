#include "Polynom.h"



Polynom::Polynom(QString poly)
{
	QStringList split = poly.split('x');
	cleny.resize(split.length());
	int counter = 0;
	for (int i = split.length() - 1; i >= 1; i--)
	{
		QStringList tmpspl = split[i].split('+');
		if (tmpspl.length() == 2)
			cleny[counter] = tmpspl[1].toDouble();
		else
		{
			tmpspl = split[i].split('-');
			cleny[counter] = -tmpspl[1].toDouble();
		}
		counter++;
	}
	cleny[counter] = split[0].toDouble();
}

Polynom::~Polynom()
{
}

void Polynom::Vypis()
{
	for(int i=cleny.size()-1;i>=0;i--)
	{
		QString tmp=cleny[i]>=0?"+":"";
		std::cout << (tmp + QString::number(cleny[i]) + "x^" + QString::number(i)).toStdString() << std::endl;
	}
}
