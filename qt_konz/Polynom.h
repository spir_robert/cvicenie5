#pragma once
#include <vector>
#include <QtCore>
#include <iostream>
class Polynom
{
public:
	Polynom(QString poly);
	~Polynom();
	void Vypis();
private:
	std::vector<double> cleny;
};

